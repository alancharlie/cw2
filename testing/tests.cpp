/*
tests.cpp
MISIS: M00734828
Created: 29/3/2021
Updated: 3/4/2021
*/

#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <iostream>
#include "Book.h"
#include "tree.h"
#include <fstream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <numeric>
#include <cmath>
#include <sstream>
#include <string>
#include <algorithm>


TEST_CASE("test1", "[test1]"){
  Tree rbtree;
  Book obj;
  Book* b = &obj;
  std::cout<<"---------------------------------------\n";
  std::cout<<"1ST TEST CASE (INSERT AND SEARCH)";
  std::cout<<"\n---------------------------------------";

  obj.setValues("binary search algorithm", "alan", "2346256345", 2);
  REQUIRE(rbtree.testInsert(b) == true );

  std::cout<<"\n\nSEARCH RESULT:";
  REQUIRE(rbtree.testSearch("binary search algorithm") == true);
}



TEST_CASE("test2", "[test2]"){
  Tree rbtree;
  
  std::cout<<"\n\n------------------------------------------\n";
  std::cout<<"2ND TEST CASE (INSERT FROM FILE AND SEARCH)";
  std::cout<<"\n------------------------------------------";
  

  std::cout<<"\n\nSEARCH RESULT:";
  REQUIRE(rbtree.testSearch("Decentralised Reinforcement Learning in Markov Games") == true);
}


TEST_CASE("test3", "[test3]"){
  Tree rbtree;
  Book obj;
  Book* b = &obj;
  std::cout<<"\n\n------------------------------------------\n";
  std::cout<<"3RD TEST CASE (INSERT AND DELETE)";
  std::cout<<"\n------------------------------------------";
  
  obj.setValues("Linear search algorithm", "charly", "4545425525", 5);
  REQUIRE(rbtree.testInsert(b) == true );
  
  
  std::cout<<"\n\nDELETE RESULT:";
  REQUIRE(rbtree.testDelete("Linear search algorithm") == true);

  
}