#ifndef _RBTREE_H_
#define _RBTREE_H_
/*
tree.h
MISIS: M00734828
Created: 28/3/2021
Updated: 3/4/2021
*/

#include "Book.h"
#include "Node.h"
#include <bits/stdc++.h>
#include <fstream>
#include <cstdio>
#include <iostream>

class Tree{
  private:
  Node* root;
  Node* TNULL;
  

  public:
  bool boolean;
  Tree();
  void insert(Book *b);
  bool testInsert(Book *b);
  void fixInsert(Node *z);
  void leftRotate(Node *x);
  void rightRotate(Node *x);
  Node* getRoot();
  Node* TreeSearch(Node *nodeptr, std::string key);
  Node* search(std::string k);
  bool testSearch(std::string key);
  Node* minimum(Node* node);
  void deleteData(std::string title);
  void deleteNode(Node* node, std::string key);
  bool testDelete(std::string title);
	void fixDelete(Node* x);
  void rbTransplant(Node* u, Node* v);
  void read_data(std::string filename);
  void write_data(std::string title, std::string author, std::string booknumber, int quantity, std::string filename);
  void delete_book_from_file(const std::string& title_to_delete, const std::string& file_name);
  std::vector<std::string> StringToVector(std::string theString, char separator);

};
#endif