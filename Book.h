#ifndef _BOOK_H_
#define _BOOK_H_
/*
Book.h
MISIS: M00734828
Created: 26/3/2021
Updated: 3/4/2021
*/

#include <iostream>
#include <string>

class Book{
private:
  std::string title;
  std::string author;
  std::string booknumber;
  int qty;
public:
  void setValues(std::string title, std::string author, std::string booknumber, int qty);
  std::string getTitle();
  std::string getAuthor();
  std::string getBooknumber();
  int getQuantity();
  
};
#endif