#ifndef _NODE_H_
#define _NODE_H_
/*
Node.h
MISIS: M00734828
Created: 28/3/2021
Updated: 3/4/2021
*/

#include <iostream>

struct Node {
    std::string title ;
    std::string author ;
    std::string booknumber ;
    int quantity ;
    Node *left = nullptr;
    Node *right = nullptr;
    Node *parent = nullptr;
    int color;  //0 is black and 1 is red
};
#endif