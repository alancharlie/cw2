/*
Book.cpp
MISIS: M00734828
Created: 26/3/2021
Updated: 3/4/2021
*/

#include "Book.h"
#include <fstream>

/*
function to set values to the data membders
@param title the title of the book
@param author the author of the book
@param booknumber the booknumber of the book
@param qty the quantity of the book
*/
void Book::setValues(std::string title, std::string author, std::string booknumber, int qty){

  this->title = title;
  this->author = author;
  this->booknumber = booknumber;
  this->qty = qty;
  
}

/*
function to return the title of book
@return the title of the book
*/
std::string Book::getTitle(){
  return title;
}

/*
function to return the author of book
@return the author of the book
*/
std::string Book::getAuthor(){
  return author;
}

/*
function to return the ISBN of book
@return the booknumber of the book
*/
std::string Book::getBooknumber(){
  return booknumber;
}

/*
function to return the quantity of book
@return the quantity of the book
*/
int Book::getQuantity(){
  return qty;
}

