/*
main.cpp
MISIS: M00734828
Created: 29/3/2021
Updated: 3/4/2021
*/

#include <iostream>
#include "Book.h"
#include "tree.h"
#include <fstream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <numeric>
#include <cmath>
#include <sstream>
#include <string>
#include <algorithm>

int main (int argc, char** argv) {
  char ans = 'q';
  Tree rb;
  if(argc == 1){
    std::cout << "\nNo command line argument found\n";
  }
  if(argc == 2){
    rb.read_data( argv[1] );

    while(ans != 'Q')
    { 
      std::cout << "\n\n\t.................";
      std::cout << "\n\t\tMAIN MENU";
      std::cout << "\n\t.................";
      std::cout << "\n\nEnter an operation (CASE-SENSITIVE): \nA) Add New Book \nB) search a book \nC) Delete book \nQ) Quit \n";
      std::cin >> ans;
      switch( ans ) {
        case 'A': {
          std::string i_name,i_author, i_isbn; 
          int i_quantity;

          std::cout << "\n\nEnter the Book's name:\n";
          std::cin.ignore();
          getline(std::cin, i_name);
          

          std::cout << "\n\nEnter the Book's Author:\n";
          getline(std::cin, i_author);

          std::cout << "\n\nEnter the Book's ISBN:\n";
          getline(std::cin, i_isbn);

          std::cout << "\n\nEnter the Book's Quantity:\n";
          std::cin>>i_quantity;
          
          rb.write_data(i_name, i_author, i_isbn, i_quantity, argv[1]);
          
        }
        break;
        case 'B': {
          std::string input;

          std::cout << "Enter the name of the book:\n";
          std::cin.ignore();
          getline(std::cin, input);

          rb.search(input);
        } 
        break;
        case 'C':{
          std::string title;

          std::cout << "Enter book name to delete the book:\n";
          std::cin.ignore();
          getline(std::cin, title);

          rb.deleteData( title );
          
          rb.delete_book_from_file( title, argv[1] );  
        }
        break;
        case 'Q':{
          std::cout << "\n\tTHANK YOU\n";
        }
        break;
        default: 
          std::cout << "Invalid command. Please try again." << std::endl;
      }
    }
  }
}
