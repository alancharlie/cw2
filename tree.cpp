 /*
tree.cpp
MISIS: M00734828
Created: 28/3/2021
Updated: 3/4/2021
*/

#include "tree.h"
#include "Book.h"

/*
Constructor 
*/
Tree::Tree() {
	TNULL = new Node;
	TNULL->color = 0;
	TNULL->left = nullptr;
	TNULL->right = nullptr;
	root = TNULL;
}

/*
function to insert data to the tree
@param b an object of Book class
*/
void Tree::insert(Book* b) {
	Node* node = new Node;
	node->parent = nullptr;
	node->title = b->getTitle();
	node->author = b->getAuthor();
	node->booknumber = b->getBooknumber();
	node->quantity = b->getQuantity();
	node->left = TNULL;
	node->right = TNULL;
	node->color = 1;       // new node is red

	Node* y = nullptr;
	Node* x = this->root;

	while (x != TNULL) {
		y = x;
		if (node->title < x->title) {        // if the inserted value is less than root node go to the left and store
			x = x->left;
    } 
    else {
			x = x->right;   // if its greater move it to the right
		}
	}

		// y is parent of x
	node->parent = y;
  //if y is null root is the node
	if (y == nullptr) {  
		root = node;
	} 
  // else if key title is less than parent y then left is considered else right
  else if (node->title < y->title) {
		y->left = node;
	} 
  else {
		y->right = node;
	}

	// if new node is a root node, simply return
	if (node->parent == nullptr){
		node->color = 0;
    boolean = true;
		return;
	}

	// if the grandparent is null, simply return
	if (node->parent->parent == nullptr) {
    boolean = true;
		return;
	}

	// Fix the tree
	fixInsert(node);
  
}

/*
function to test the insert operation
@param b object of class book
*/
bool Tree::testInsert(Book *b){
insert(b);
  if(boolean == true){
    std::cout<<"\nSuccessfully inserted\n";
    return true;
  }
  else
  {
    std::cout<<"testing failed\n";
       return false;
  }
}
  
//fixes the red-black tree once the element or data is inserted
void Tree::fixInsert(Node* z){
	Node* u;
  // when z's parent color is red
	while (z->parent->color == 1) {
		if (z->parent == z->parent->parent->right) { 
			u = z->parent->parent->left; // uncle
			if (u->color == 1) {
			  u->color = 0;
				z->parent->color = 0;
				z->parent->parent->color = 1;
				z = z->parent->parent;
			} 
      else {
				if (z == z->parent->left) {
					z = z->parent;
					rightRotate(z);
				}
				z->parent->color = 0;
				z->parent->parent->color = 1;
				leftRotate(z->parent->parent);
			}
			} 
      else {
				u = z->parent->parent->right; // uncle

				if (u->color == 1) {
					u->color = 0;
					z->parent->color = 0;
					z->parent->parent->color = 1;
					z = z->parent->parent;	
				} 
        else {
					if (z == z->parent->right) {
						z = z->parent;
						leftRotate(z);
					}
					z->parent->color = 0;
					z->parent->parent->color = 1;
					rightRotate(z->parent->parent);
				}
			}
			if (z == root) {
				break;
			}
	}
	root->color = 0;
}
  
/*
function to perform left rotation
@param x pointing towards the inserted data
*/
void Tree::leftRotate(Node* x) {
	Node* y = x->right;
	x->right = y->left;
	if (y->left != TNULL) {
		y->left->parent = x;
	}
	y->parent = x->parent;
	if (x->parent == nullptr) {
		this->root = y;
	} 
  else if (x == x->parent->left) {
		x->parent->left = y;
	} 
  else {
		x->parent->right = y;
	}
		y->left = x;
		x->parent = y;
}

/*
function to perform right roatation
@param x pointing towards the inserted data
*/
void Tree::rightRotate(Node* x) {
	Node* y = x->left;
	x->left = y->right;
	if (y->right != TNULL) {
		y->right->parent = x;
	}
	y->parent = x->parent;
	if (x->parent == nullptr) {
		this->root = y;
	} 
  else if (x == x->parent->right) {
		x->parent->right = y;
	} 
  else {
		x->parent->left = y;
	}
	y->right = x;
	x->parent = y;
}

/*
function sed to return the root of the tree
@return the root node of the tree
*/
Node* Tree::getRoot(){
	return this->root;
}

/*
function to search for a book
@param nodeptr pointing towards the inserted data
@param key the title of the book used for searching
@return data if its found or returns nullptr
*/
Node* Tree::TreeSearch(Node* nodeptr, std::string key) {
  
  if(nodeptr == nullptr) { 
    std::cout<<"\n\nDATA NOT FOUND\n";
    boolean = true;
    return nullptr; 
  }

  while(nodeptr) {
    if(key == nodeptr->title){ 
      std::cout<<"\nTITLE: "<<nodeptr->title;
      std::cout<<"\nAUTHOR: "<<nodeptr->author;
      std::cout<<"\nISBN: "<<nodeptr->booknumber;
      std::cout<<"\nQUANTITY: "<<nodeptr->quantity<<std::endl;
      boolean = true;
      return nullptr; 
    }
    else if(key < nodeptr->title){
      return TreeSearch(nodeptr->left, key);
    }
    else if(key > nodeptr->title) { 
      return TreeSearch(nodeptr->right, key);
    }
  }
  return nullptr;
}

/*
function to search for a book 
@param key the title of the booknumber
@return the function TreeSearch 
*/ 
Node* Tree::search(std::string key){
  return TreeSearch(this->root, key);
}

/*
function to test the search operation
@param key title of the book
*/
bool Tree::testSearch(std::string key){
  read_data("books");
  search(key);
  if(boolean == 1){
    return true;
  }
  else
  {
    std::cout<<"testing failed\n";
       return false;
  }
}
 

/*
function used to find the minimum node 
@param node 
@return the minimum node if found
*/
Node* Tree::minimum(Node* node) {
	while (node->left != TNULL) {
		node = node->left;
	}
	return node;
}

/*
function used to delete the data from the tree
@param title the title of the book the user wants to delete
*/
void Tree::deleteData(std::string title) {
	deleteNode(this->root, title);
}

/*
function used to delete the node from the tree
@param node pointing to the root node
@param key the title of the book
*/
void Tree::deleteNode(Node* node, std::string key) {
	// find the node containing key
	Node* z = TNULL;
	Node* x; 
  Node* y;
	while (node != TNULL){
		if (node->title == key) {
			z = node;
		}

		if (node->title <= key) {
			node = node->right;
		} 
    else {
			node = node->left;
		}
	}

	if (z == TNULL) {
		std::cout<<"\nCouldn't find the book in the Data structure"<<std::endl;
    boolean = true;
		return;
	} 

	y = z;
	int y_original_color = y->color;
	if (z->left == TNULL) {
		x = z->right;
		rbTransplant(z, z->right);
    std::cout<<"\nSuccessfully Deleted."<<std::endl;
	} 
  else if (z->right == TNULL) {
		x = z->left;
		rbTransplant(z, z->left);
    std::cout<<"\nSuccessfully Deleted."<<std::endl;
	}
  else {
		y = minimum(z->right);
		y_original_color = y->color;
		x = y->right;
		if (y->parent == z) {
			x->parent = y;
      std::cout<<"\nSuccessfully Deleted."<<std::endl;
		}
    else {
			rbTransplant(y, y->right);
			y->right = z->right;
			y->right->parent = y;
      std::cout<<"\nSuccessfully Deleted."<<std::endl;
		}

		rbTransplant(z, y);
		y->left = z->left;
		y->left->parent = y;
		y->color = z->color;
	}
	delete z;
	if (y_original_color == 0){
		fixDelete(x);
	}
}

/*
function to test the delete operation
@param title title of the book
*/
bool Tree::testDelete(std::string title){
  deleteData(title);
  if(boolean == true){
    return true;
  }
  else
  {
    std::cout<<"testing failed\n";
       return false;
  }
}
	
/*
the function fixes the tree once the preferred data is deleted
@param x points to the node
*/
void Tree::fixDelete(Node* x) {
	Node* s;
	while (x != root && x->color == 0) {
		if (x == x->parent->left) {
			s = x->parent->right;
			if (s->color == 1) {
				s->color = 0;
				x->parent->color = 1;
				leftRotate(x->parent);
				s = x->parent->right;
			}

			if (s->left->color == 0 && s->right->color == 0) {
				s->color = 1;
				x = x->parent;
			} 
      else {
				if (s->right->color == 0) {
					s->left->color = 0;
					s->color = 1;
					rightRotate(s);
					s = x->parent->right;
				} 

				s->color = x->parent->color;
				x->parent->color = 0;
				s->right->color = 0;
				leftRotate(x->parent);
				x = root;
			}
		}
    else {
		  s = x->parent->left;
			if (s->color == 1) {
				s->color = 0;
				x->parent->color = 1;
				rightRotate(x->parent);
				s = x->parent->left;
			}

			if (s->right->color == 0 && s->right->color == 0) {
				s->color = 1;
				x = x->parent;
			} 
      else {
				if (s->left->color == 0) {
					s->right->color = 0;
					s->color = 1;
					leftRotate(s);
					s = x->parent->left;
				} 

				s->color = x->parent->color;
				x->parent->color = 0;
				s->left->color = 0;
				rightRotate(x->parent);
				x = root;
			}
		} 
	}
	x->color = 0;
}

/*
function to replace nodes
@param u
@param v
*/
void Tree::rbTransplant(Node* u, Node* v){
	if (u->parent == nullptr) {
		root = v;
	} 
  else if (u == u->parent->left){
		u->parent->left = v;
	} 
  else {
		u->parent->right = v;
	}
	v->parent = u->parent;
}


/*
function used to read data from the textfile
@param filename name of the file retrieved from the command line
*/
void Tree::read_data(std::string filename){
Book book_obj;
Book* b = &book_obj;
std::ifstream ifile;
std::string txtFromFile = " ";
ifile.open(filename, std::ios_base::in);
if(ifile.is_open()){
  while(ifile.good()){
    getline(ifile, txtFromFile);
    std::vector<std::string> vect = StringToVector(txtFromFile, '\t');
    
    if(ifile.good()){  
      book_obj.setValues(vect[0], vect[1], vect[2], stoi(vect[3]));
      insert(b);
    }
  }  
std::cout<<"\nFile successfully uploaded"; 
ifile.close();
}
else{
  std::cout<<"\n!!FILE NOT FOUND PLEASE RE-RUN WITH PROPER FILE NAME!!";
}
}

/*
function used to write data to the textfile
@param title the name of the book entered by the user
@param author the author entered by the user
@param booknumber the ISBN entered by the user
@param quantity the quantity entered by the user 
@param filename name of the file retrieved from the command line
*/
void Tree::write_data(std::string title,std::string author,std::string booknumber,int quantity,std::string filename){
  Book obj;
  Book *b = &obj;
  b->setValues(title, author, booknumber, quantity);
  insert(b);
  std::ofstream file(filename, std::ios::app);
  file<<title<<'\t'<<author<<'\t'<<booknumber<<'\t'<<quantity<<std::endl;
}

/*
function used to delete data from the textfile
@param title_to_delete the name of the book entered by the user
@param file_name the name retrieved from the command line 
*/
void Tree::delete_book_from_file(const std::string& title_to_delete, const std::string& file_name) {
  std::ifstream file(file_name);
  std::ofstream temp("temp.txt");
  std::string line_buffer;
  std::string column_buffer;
  
  while (std::getline(file, line_buffer)) {
    std::istringstream line_stream(line_buffer);
    std::getline(line_stream, column_buffer, '\t');   // now column_buffer is the title of this line's book
    if (column_buffer != title_to_delete) {
      temp << line_buffer << std::endl;          //writes the data from the file to a temporary file.
    }
  }    

remove(file_name.c_str());                      //removes the file with the content the user requested to delete
rename("temp.txt", file_name.c_str());    //the temp file with all the data is renamed to the previous filename.
}


std::vector<std::string> Tree::StringToVector(std::string theString, char separator) {
 
  std::vector<std::string> words;
  std::stringstream ss(theString);
  std::string sIndivStr;
  while(getline(ss, sIndivStr, separator)){
    words.push_back(sIndivStr);
  }
  return words;
}